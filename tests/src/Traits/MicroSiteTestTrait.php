<?php

namespace Drupal\Tests\micro_site\Traits;

use Drupal\Core\Session\AccountInterface;

/**
 * Contains helper classes for tests to set up various configuration.
 */
trait MicroSiteTestTrait {

  /**
   * Adds a test micro site to an entity.
   *
   * @param string $entity_type
   *   The entity type being acted upon.
   * @param int $entity_id
   *   The entity id.
   * @param array|string $ids
   *   An id or array of ids of micro site to add.
   * @param string $field
   *   The name of the micro site field used to attach to the entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addMicroSiteToEntity($entity_type, $entity_id, $ids, $field) {
    if ($entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id)) {
      $entity->set($field, $ids);
      $entity->save();
    }
  }

  /**
   * Returns a list of all micro sites.
   *
   * @return \Drupal\micro_site\Entity\SiteInterface[]
   *   An array of micro site entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMicroSites() {
    /** @var \Drupal\micro_site\SiteStorageInterface $site_storage */
    $site_storage = \Drupal::entityTypeManager()->getStorage('site');
    $site_storage->resetCache();
    return $site_storage->loadMultiple();
  }

  /**
   * Generates site entities for testing.
   *
   * In my environment, I use the microsite.local hostname as a base. Then I name
   * hostnames one.* two.* up to five.
   *
   * The script may also add test6.local, test7.local, test8.local up to any
   * number to test a large number of micro sites.
   *
   * @param string $type
   *   The micro site type.
   * @param int $count
   *   The number of micro site to create.
   * @param string $type_url
   *   The type of the URL.
   * @param string|null $base_url
   *   The base url to use for micro site creation (e.g. microsite.local).
   * @param array $list
   *   An optional list of sub-domains to apply instead of the default set.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createSites($type, $count = 1, $type_url = 'domain', $micro_site_base_url = NULL, array $list = []) {
    if (empty($micro_site_base_url)) {
      $micro_site_base_url = $this->microSiteBaseUrl;
    }
    // Note: these micro sites are rigged to work on my test server.
    // For proper testing, yours should be set up similarly, but you can pass a
    // $list array to change the default.
    if (empty($list)) {
      $list = [
        'one',
        'two',
        'three',
        'four',
        'five',
      ];
    }
    for ($i = 0; $i < $count; $i++) {
      if (!empty($list[$i])) {
        // Currently we only support the domain option. So we build the same URL
        // whatever the type url is set. We could later test also sub-domain VS
        // domain (and maybe VS path) based micro sites.
        if ($type_url == 'subdomain') {
          $site_url = $list[$i] . '.' . $micro_site_base_url;
        }
        elseif ($type_url == 'domain') {
          $site_url = $list[$i] . '.' . $micro_site_base_url;
        }
        else {
          $site_url = $list[$i] . '.' . $micro_site_base_url;
        }
        $site_mail = $list[$i] . '@' . $micro_site_base_url;
        $name = 'Micro Site ' . ucfirst($list[$i]);
      }
      else {
        $site_url = 'test' . $i . '.local';
        $site_mail = 'mail@test' . $i . '.local';
        $name = 'Micro Site Test ' . $i;
      }

      $values = [
        'type' => $type,
        'site_url' => $site_url,
        'mail' => $site_mail,
        'site_scheme' => FALSE,
        'name' => $name,
      ];
      $micro_site = \Drupal::entityTypeManager()->getStorage('site')->create($values);
      $micro_site->save();
    }
  }

  /**
   * Create a site type.
   *
   * @param $id
   * @param $label
   * @param string $description
   * @param false $menu
   * @param false $vocabulary
   * @param array $types
   * @param array $typesTab
   * @param array $vocabularies
   * @param bool $usersManagement
   *
   * @return \Drupal\micro_site\Entity\SiteTypeInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createSiteType($id, $label, $description = '', $menu = FALSE, $vocabulary = FALSE, $types = [], $typesTab = [], $vocabularies = [], $usersManagement = TRUE) {
    $values = [
      'id' => $id,
      'label' => $label,
      'description' => $description,
      'menu' => $menu,
      'vocabulary' => $vocabulary,
      'types' => $types,
      'typesTab' => $typesTab,
      'vocabularies' => $vocabularies,
      'usersManagement' => $usersManagement,
    ];
    $site_type = \Drupal::entityTypeManager()->getStorage('site_type')->create($values);
    $site_type->save();
    return $site_type;
  }

  /**
   * Generates site entities for testing.
   *
   * In my environment, I use the microsite.local hostname as a base. Then I name
   * hostnames one.* two.* up to five.
   *
   * The script may also add test6.local, test7.local, test8.local up to any
   * number to test a large number of micro sites.
   *
   * @param string $type
   *   The micro site type.
   * @param string $site_url
   *   The site url.
   * @param string $type_url
   *   The type of the URL.
   * @param string $name
   *   The site name.
   * @param string $slogan
   *   The site slogan
   * @param string $site_mail
   *   The site mail.
   * @param array $settings
   *   Others settings for the site.
   *
   * @return \Drupal\micro_site\Entity\SiteInterface
   *   The micro site.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createSite($type, $name, $slogan, $type_url, $site_url, $site_mail, $settings = []) {
    $values = [
      'type' => $type,
      'type_url' => $type_url,
      'site_url' => $site_url,
      'mail' => $site_mail,
      'site_scheme' => FALSE,
      'name' => $name,
      'slogan' => $slogan
    ];
    $values = $values + $settings;
    $micro_site = \Drupal::entityTypeManager()->getStorage('site')->create($values);
    $micro_site->save();
    return $micro_site;
  }

}
