<?php

namespace Drupal\Tests\micro_site\Functional;

use Drupal\Core\Url;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group micro_site
 */
class BaseTest extends MicroSiteBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'micro_site',
  ];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The theme to install as the default for testing.
   *
   * Defaults to the install profile's default theme, if it specifies any.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->createUserWithPassword(['view published site entities']);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testHome() {
    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests micro site access.
   */
  public function testSiteAccess() {
    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->assertSession()->statusCodeEquals(200);

    $generic = $this->createSiteType('generic', 'Generic', 'Generic site type');
    $settings = ['user_id' => $this->microSiteAdminUser->id(), 'registered' => TRUE];
    $site_one = $this->createSite('generic', 'Site One', 'Site one slogan', 'domain', 'one.microsite.local', 'siteone@microsite.local', $settings);
    $expected = 'http://one.microsite.local';
    $this->assertEquals($site_one->getSitePath(), $expected);

    // Nobody is logged in on the site one.
    $this->drupalGet(Url::fromUri($site_one->getSitePath()));
    $this->assertSession()->statusCodeEquals(403);
    // TODO: Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
    // Verify the assertion: pageTextContains() for HTML responses, responseContains() for non-HTML responses.
    // The passed text should be HTML decoded, exactly as a human sees it in the browser.
    $this->assertSession()->pageTextContains('Site One');

    $this->fillField('edit-name', $this->globalAdminUser->getAccountName());
    $this->fillField('edit-pass', $this->password);
    $this->pressButton('edit-submit');
    $this->drupalUserIsLoggedIn($this->globalAdminUser);
    $this->assertSession()->statusCodeEquals(200);

    // Reload the master.
    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet(Url::fromUri($site_one->getSitePath()));
    $this->assertSession()->statusCodeEquals(200);

    $this->clickLink('Edit');
    $this->checkField('edit-status-value');
    $this->pressButton('Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Saved the ' . $site_one->label() .' Site.');

    $this->clickLink('Log out');
    $this->drupalGet(Url::fromUri($site_one->getSitePath()));
    $this->assertSession()->statusCodeEquals(200);

    $this->fillField('edit-name', $this->user->getAccountName());
    $this->fillField('edit-pass', $this->password);
    $this->pressButton('edit-submit');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($site_one->getSitePath() . "/site/{$site_one->id()}/edit");
    $this->assertSession()->statusCodeEquals(403);

    $this->logOutLogInUser($this->microSiteOwnerUser);
    $this->drupalGet($site_one->getSitePath() . "/site/{$site_one->id()}/edit");
    $this->assertSession()->statusCodeEquals(403);

    $this->logOutLogInUser($this->globalAdminUser);
    // Add owner user as site admin.
    $edit['field_site_administrator[0][target_id]'] = $this->microSiteOwnerUser->getAccountName() . ' ('. $this->microSiteOwnerUser->id() . ')';
    $this->drupalGet($site_one->getSitePath() ."/site/{$site_one->id()}/edit");
    $this->submitForm($edit, 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($site_one->getSitePath() . "/site/{$site_one->id()}/edit");

    $this->logOutLogInUser($this->microSiteOwnerUser);
    $this->drupalGet($site_one->getSitePath() . "/site/{$site_one->id()}/edit");
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests micro site asset.
   */
  public function testSiteAsset() {
    $generic = $this->createSiteType('generic', 'Generic', 'Generic site type');
    $settings = [
      'user_id' => $this->microSiteAdminUser->id(),
      'registered' => TRUE,
      'status' => TRUE,
    ];
    $site_one = $this->createSite('generic', 'Site One', 'Site one slogan', 'domain', 'one.microsite.local', 'siteone@microsite.local', $settings);
    $site_one->setCss('body {background-color: red !important;');
    $expected = 'http://one.microsite.local';
    $this->assertEquals($site_one->getSitePath(), $expected);
    $name = 'site-' . $site_one->id();
    $css_file = $name . '-' . md5($name) . '.css';
    $this->drupalGet(Url::fromUri($site_one->getSitePath()));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains($css_file);
    $xpath = $this->xpath("//link[contains(@href, '" . $css_file . "')]");
    $this->assertEquals(count($xpath), 1, 'Exactly one ' . $css_file . ' css file asset found.');
  }

}
