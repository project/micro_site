<?php

namespace Drupal\Tests\micro_site\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\micro_site\Traits\MicroSiteTestTrait;
use Drupal\user\RoleInterface;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group micro_site
 */
abstract class MicroSiteBase extends BrowserTestBase {

  use MicroSiteTestTrait;

  /**
   * We use the standard profile for testing.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * A user with permission to administer micro site.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $globalAdminUser;

  /**
   * A user with permission to administer own microsite.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $microSiteAdminUser;

  /**
   * A user with permission to view and edit own microsite.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $microSiteOwnerUser;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\micro_site\SiteStorageInterface definition.
   *
   * @var \Drupal\micro_site\SiteStorageInterface
   */
  protected $siteStorage;

  /**
   * The base URL used for micro site based on a sub domain.
   *
   * @var string
   */
  protected $microSiteBaseUrl;

  /**
   * The base Scheme of the micro site master host.
   *
   * @var string
   */
  protected $microSiteBaseScheme;

  /**
   * The public URL of the micro site master host.
   *
   * @var string
   */
  protected $microSitePublicUrl;

  /**
   * The master URL of the micro site master host.
   *
   * @var string
   */
  protected $masterUrl;

  /**
   * A default password.
   *
   * @var string
   */
  protected $password = 'password';

  /**
   * The theme to install as the default for testing.
   *
   * Defaults to the install profile's default theme, if it specifies any.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->configFactory = $this->container->get('config.factory');
    $this->siteStorage = $this->entityTypeManager->getStorage('site');
    $this->globalAdminUser = $this->createUserWithPassword($this->getGlobalAdminPermissions());
    $this->microSiteAdminUser = $this->createUserWithPassword($this->getMicroSiteAdminPermissions());
    $this->microSiteOwnerUser = $this->createUserWithPassword($this->getMicroSiteOwnerPermissions());
    $this->microSiteBaseUrl = 'microsite.local';
    $this->microSiteBaseScheme = 'http';
    $this->microSitePublicUrl = 'www.microsite.local';
    $this->masterUrl = $this->microSiteBaseScheme . '://' . $this->microSitePublicUrl;
    $config = \Drupal::configFactory()->getEditable('micro_site.settings');
    $config->set('base_url', $this->microSiteBaseUrl);
    $config->set('base_scheme', $this->microSiteBaseScheme);
    $config->set('public_url', $this->microSitePublicUrl);
    $config->save(TRUE);
    $this->drupalPlaceBlock('user_login_block');
    $this->drupalPlaceBlock('micro_site_information', ['display_active_site' => TRUE]);
    // Big Pipe send 403 from its controller on micro sites.
    $this->container->get('module_installer')->uninstall(['big_pipe']);
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['view published site entities']);
  }

  /**
   * Gets the permissions for the global administrator.
   *
   * @return string[]
   *   The permissions.
   */
  protected function getGlobalAdminPermissions() {
    return [
      'administer site entities',
      'administer micro site settings',
      'view micro site information',
      'view unpublished site entities',
      'view published site entities',
      'edit site entities',
    ];
  }

  /**
   * Gets the permissions for a micro site administrator.
   *
   * @return string[]
   *   The permissions.
   */
  protected function getMicroSiteAdminPermissions() {
    return [
      'administer own site entity',
      'view micro site information',
      'view own unpublished site entity',
      'edit own site entity',
      'view published site entities',
    ];
  }

  /**
   * Gets the permissions for a micro site administrator.
   *
   * @return string[]
   *   The permissions.
   */
  protected function getMicroSiteOwnerPermissions() {
    return [
      'view own unpublished site entity',
      'view published site entities',
      'edit own site entity',
      'view micro site information',
    ];
  }

  /**
   * Finds link with specified locator.
   *
   * @param string $locator
   *   Link id, title, text or image alt.
   *
   * @return \Behat\Mink\Element\NodeElement|null
   *   The link node element.
   */
  public function findLink($locator) {
    return $this->getSession()->getPage()->findLink($locator);
  }

  /**
   * Confirms absence of link with specified locator.
   *
   * @param string $locator
   *   Link id, title, text or image alt.
   *
   * @return bool
   *   TRUE if link is absent, or FALSE.
   */
  public function findNoLink($locator) {
    return empty($this->getSession()->getPage()->hasLink($locator));
  }

  /**
   * Finds field (input, textarea, select) with specified locator.
   *
   * @param string $locator
   *   Input id, name or label.
   *
   * @return \Behat\Mink\Element\NodeElement|null
   *   The input field element.
   */
  public function findField($locator) {
    return $this->getSession()->getPage()->findField($locator);
  }

  /**
   * Finds button with specified locator.
   *
   * @param string $locator
   *   Button id, value or alt.
   *
   * @return \Behat\Mink\Element\NodeElement|null
   *   The button node element.
   */
  public function findButton($locator) {
    return $this->getSession()->getPage()->findButton($locator);
  }

  /**
   * Presses button with specified locator.
   *
   * @param string $locator
   *   Button id, value or alt.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function pressButton($locator) {
    $this->getSession()->getPage()->pressButton($locator);
  }

  /**
   * Fills in field (input, textarea, select) with specified locator.
   *
   * @param string $locator
   *   Input id, name or label.
   * @param string $value
   *   Value.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   *
   * @see \Behat\Mink\Element\NodeElement::setValue
   */
  public function fillField($locator, $value) {
    $this->getSession()->getPage()->fillField($locator, $value);
  }

  /**
   * Checks checkbox with specified locator.
   *
   * @param string $locator
   *   An input id, name or label.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function checkField($locator) {
    $this->getSession()->getPage()->checkField($locator);
  }

  /**
   * Unchecks checkbox with specified locator.
   *
   * @param string $locator
   *   An input id, name or label.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function uncheckField($locator) {
    $this->getSession()->getPage()->uncheckField($locator);
  }

  /**
   * Selects option from select field with specified locator.
   *
   * @param string $locator
   *   An input id, name or label.
   * @param string $value
   *   The option value.
   * @param bool $multiple
   *   Whether to select multiple options.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   *
   * @see NodeElement::selectOption
   */
  public function selectFieldOption($locator, $value, $multiple = FALSE) {
    $this->getSession()->getPage()->selectFieldOption($locator, $value, $multiple);
  }

  /**
   * Create user with password password.
   *
   * @param array $permissions
   * @param null $name
   * @param false $admin
   * @param array $values
   *
   * @return \Drupal\user\Entity\User|false
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createUserWithPassword(array $permissions = [], $name = NULL, $admin = FALSE, array $values = []) {
    $values_password = [
      'pass' => $this->password,
    ];
    $values = $values_password + $values;
    return $this->drupalCreateUser($permissions, $name, $admin, $values);
  }

  /**
   * Log out and Log in a user with click.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function logOutLogInUser(AccountInterface $user) {
    $this->clickLink('Log out');
    $this->fillField('edit-name', $user->getAccountName());
    $this->fillField('edit-pass', $this->password);
    $this->pressButton('edit-submit');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Log in a user with click.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   * @param boolean $check_status_code
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function logInUser(AccountInterface $user, $check_status_code = TRUE) {
    $this->fillField('edit-name', $user->getAccountName());
    $this->fillField('edit-pass', $this->password);
    $this->pressButton('edit-submit');
    if ($check_status_code) {
      $this->assertSession()->statusCodeEquals(200);
    }
  }

}
