<?php

namespace Drupal\micro_site\PathProcessor;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\micro_site\Entity\SiteInterface;
use Drupal\micro_site\SiteNegotiatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Process the Inbound and Outbound front page urls per micro site.
 */
class SiteFrontPageProcessor implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * A config factory for retrieving required config settings.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The Site negotiator.
   *
   * @var \Drupal\micro_site\SiteNegotiatorInterface
   */
  protected $negotiator;

  /**
   * Constructs the site front page processor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\micro_site\SiteNegotiatorInterface $site_negotiator
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, SiteNegotiatorInterface $site_negotiator ) {
    $this->config = $config_factory;
    $this->negotiator = $site_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if ($path === '/') {
      // Check if a site exists and has the site url requested.
      $http_post = $request->server->get('HTTP_HOST');
      $public_url = $this->config->get('micro_site.settings')->get('public_url');
      if ($http_post == $public_url) {
        // Nothing to do.
        return $path;
      }

      $base_url = $this->config->get('micro_site.settings')->get('base_url');
      if ($http_post == $base_url) {
        // Nothing to do.
        return $path;
      }

      if ($http_post == 'localhost') {
        return $path;
      }
      if ($site = $this->negotiator->getActiveSite()) {
        $path = '/site/' . $site->id();
      }
      else {
        throw new NotFoundHttpException;
      }
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (preg_match('/^\/site\/([0-9]+)$/i', $path, $matches)) {
      $site_id = $matches['1'];
      $site = $this->negotiator->loadById($site_id);
      $active_site = $this->negotiator->getActiveSite();
      if ($site instanceof SiteInterface && $active_site && $active_site->id() == $site->id()) {
        $path = '/';
      }
    }
    return $path;
  }

}
